 
import hazi.Game;
import hazi.GamePanel;
import hazi.HighScore;
import hazi.Racket;

import java.awt.Color;
import java.awt.Rectangle;

import javax.swing.JFrame;

import org.junit.Assert;
import org.junit.Test;
 
public class Tester { 
 
 @Test 
 public void test1() { 
	 Game frame = new Game("Breakout");

     frame.setSize(350, 350);
     frame.setLocationRelativeTo(null);
     frame.setVisible(true);
     frame.setResizable(false);
     frame.setBackground(Color.black);
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.startGame();
     Assert.assertTrue(frame.isRunning());
       
 } 
 
 @Test 
 public void test2() {
	 HighScore high = new HighScore(null, null, 100);
	 boolean result = high.isEmpty();
	 Assert.assertFalse(result);
 }
 
 public void test3() {
	 HighScore high = new HighScore(null, null, 100);
	 int minscore = high.minscore();
	 Assert.assertTrue((minscore >= 0));
 }
 

 @Test 
 public void test4() {
	 GamePanel game = new GamePanel(null);
	 game.init();
	 int ballNum = game.getBallNum();
	 Assert.assertEquals(1, ballNum);
 }
 
 @Test 
 public void test5() {
	 Racket r = new Racket(null);
	 Rectangle rect = r.getBounds();
	 Assert.assertNotEquals(null, rect);
 }
 
 

 
} 
 