package hazi;

import hazi.Block.Type;

import java.awt.*;


public class Extra {
    Type type;
    GamePanel game;
    int x;
    int y;

    public Extra(Type t, GamePanel g, int x, int y) {
        type = t;
        game = g;
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g) {

        switch (type) {
            case multiball:
                g.setColor(Color.green);
                break;
            case big:
                g.setColor(Color.blue);
                break;
            case small:
                g.setColor(Color.red);
                break;
            case reverse:
                g.setColor(new Color(153, 0, 76));
                break;
            case sticky:
                g.setColor(Color.yellow);
                break;
            default:
                break;
        }

        g.fillOval((int) x, (int) y, 8, 8);

    }

    public void move() {
        y++;
    }

    public void check() {
        if (collision()) {
            GamePanel.score += 50;

            switch (type) {
                case multiball:
                    if (game.getBallNum() == 1) {
                    	game.racket.reversed = false;
                        game.racket.sticky = false;
                        Ball temp = new Ball(game);
                        temp.x = game.balls.get(0).x;
                        temp.y = game.balls.get(0).y;
                        temp.xa = game.balls.get(0).xa * 1.2;
                        temp.ya = game.balls.get(0).ya / 1.2;
                        game.balls.add(temp);
                        temp = new Ball(game);
                        temp.x = game.balls.get(0).x;
                        temp.y = game.balls.get(0).y;
                        temp.xa = game.balls.get(0).xa / 1.2;
                        temp.ya = game.balls.get(0).ya * 1.2;
                        game.balls.add(temp);
                    }
                    
                    game.extra.remove(this);
                    break;
                case big:
                    if (game.racket.width <= game.racket.widthOriginal) {
                        game.racket.width = game.racket.widthOriginal + 30;
                        game.racket.time = System.currentTimeMillis();
                        game.racket.reversed = false;
                        game.racket.sticky = false;
                        for (int i = 0; i < game.balls.size(); i++) {
                            game.balls.get(i).unstuck();
                        }
                    }
                    game.extra.remove(this);
                    break;
                case small:
                    if (game.racket.width >= game.racket.widthOriginal) {
                        game.racket.width = game.racket.widthOriginal - 30;
                        game.racket.time = System.currentTimeMillis();
                        game.racket.reversed = false;
                        game.racket.sticky = false;
                        for (int i = 0; i < game.balls.size(); i++) {
                            game.balls.get(i).unstuck();
                        }
                    }
                    game.extra.remove(this);
                    break;

                case reverse:

                    if (!game.racket.reversed) {
                        game.racket.reversed = true;
                        game.racket.sticky = false;
                        for (int i = 0; i < game.balls.size(); i++) {
                            game.balls.get(i).unstuck();
                        }
                        game.racket.width = game.racket.widthOriginal;
                        game.racket.time = System.currentTimeMillis();
                    }
                    game.extra.remove(this);
                    break;

                case sticky:

                    if (!game.racket.sticky) {
                        game.racket.sticky = true;
                        game.racket.reversed = false;
                        game.racket.width = game.racket.widthOriginal;
                        game.racket.time = System.currentTimeMillis();
                    }
                    game.extra.remove(this);
                    break;
			default:
				break;
            }
        }
        if (y + 1 > game.getHeight() - 8) {
            game.extra.remove(this);
        }
    }

    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, 8, 8);
    }

    boolean collision() {
        return game.racket.getBounds().intersects(getBounds());
    }

}
