package hazi;

import javax.swing.*;
import java.awt.*;

public class Game extends JFrame {

    final static int windowWidth = 500;
    final static int windowHeight = 550;
    GamePanel g;
    Thread game;
    public Menu menu;
    HighScore highscore;

    public Game(String title) {
        super(title);
    }
    
    public boolean isRunning() {
    	return game.isAlive();
    }
    
    public void startGame() {
        final Game frame = new Game("Breakout");

        frame.setSize(windowWidth, windowHeight);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setBackground(Color.black);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.g = new GamePanel(frame);

        frame.add(frame.g);
        frame.g.addKeyListener(frame.g);
        game = new Thread(frame.g);
        game.setName("Game thread");

        game.start();
        setVisible(false);

    }

    public void showHighScore(int score) {
        final Game frame = new Game("Breakout");
        frame.setSize(windowWidth, windowHeight);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setBackground(Color.black);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        highscore = new HighScore(this,frame,score);
        frame.add(highscore);
        frame.addKeyListener(highscore);
        frame.setVisible(true);
        dispose();

    }

    public static void main(String[] args) throws InterruptedException {

        Game frame = new Game("Breakout");

        frame.setSize(350, 350);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setBackground(Color.black);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        frame.menu = new Menu(frame);
        frame.add(frame.menu);
        frame.addKeyListener(frame.menu);
    }
}
