package hazi;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Menu extends JPanel implements KeyListener {

	private Game game;
    private double black = 200;
    private double add = 4;
    private int selected = 0;
    
    public class texts {
		String string;
		int x, y;

		public texts(String string, int x, int y) {
			this.string = string;
			this.x = x;
			this.y = y;
		}

	}
	
	public Menu(Game g) {
		game = g;
	}

	private texts[] text = {
            new texts("New Game", 40, 80),
            new texts("High Scores", 20, 180),
            new texts("Exit", 120, 280)
    };
	
	public int getTextsLength() {
		return text.length;
	}
	
	

	public void paint(Graphics g) {

		super.paint(g);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        this.setBackground(Color.black);
		g2d.setFont(new Font("Arial", Font.BOLD, 50));
		g2d.setColor(Color.white);

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (black >= 250 || black <= 60)
            add *= -1;
        black += add;


		for (int i = 0; i < text.length; i++) {
			if (i == selected) {
				g2d.setColor(new Color((int)black,(int)black,(int)black));
			} else {
				g2d.setColor(Color.white);
			}
			g2d.drawString(text[i].string, text[i].x, text[i].y);
		}
        repaint();	

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            switch (selected) {
                case 0:
                    game.startGame();
                    break;
                case 1:
                    game.showHighScore(-10);
                    break;
                case 2:
                    System.exit(0);
            }

		}				
		
		if (e.getKeyCode() == KeyEvent.VK_UP)
			selected--;
		if (e.getKeyCode() == KeyEvent.VK_DOWN)
			selected++;
		
		selected = (selected + 3) % 3;
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
