package hazi;

import java.awt.*;

public class Text {

	GamePanel game;

	public Text(GamePanel game) {
		this.game = game;
	}

	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Font font = new Font("Arial", Font.PLAIN, 20);

		g2d.setFont(font);
		g2d.setColor(Color.white);
		FontMetrics fm = g.getFontMetrics();

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.drawString("Score: ", 10, 30);
		g2d.drawString(Integer.toString(GamePanel.score),
				140 - fm.stringWidth(Integer.toString(GamePanel.score)), 30);
		g2d.drawString("Lives: ", game.getWidth() - 170, 30);
		g2d.drawString(Integer.toString(GamePanel.lives), game.getWidth() - 100 , 30);
	}
}
