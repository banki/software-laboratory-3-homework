package hazi;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Racket {
    int width = 100;
    final int widthOriginal = 100;
    public int x;
    int y;
    int xa = 0;
    private GamePanel game;
    boolean isLeftDown = false, isRightDown = false;
    long time = 0;
    boolean reversed = false;
    boolean sticky = false;

    public Racket(GamePanel game) {
        this.game = game;
    }

    void move() {

        x = x + xa;
        if (x + xa < width / 2 + 5)
            x = width / 2 + 5;
        if (x + xa > Game.windowWidth - width / 2 - 10)
            x = Game.windowWidth - width / 2 - 10;

    }

    void check() {

        if (width != widthOriginal || reversed || sticky) {
            if (time == 0) {
                time = System.currentTimeMillis();
            } else {
                if (System.currentTimeMillis() - time > 10000) {
                    time = 0;
                    width = widthOriginal;
                    reversed = false;
                    sticky = false;

                    for (int i = 0; i < game.balls.size(); i++) {
                        game.balls.get(i).unstuck();
                    }
                }
            }
        }
    }

    public void paint(Graphics2D g) {
        if (width == widthOriginal)
            g.setColor(Color.gray);
        else if (width > widthOriginal)
            g.setColor(Color.blue);
        else
            g.setColor(Color.red);
        if (reversed)
            g.setColor(new Color(153, 0, 76));
        if (sticky)
            g.setColor(Color.yellow);
        g.fillRoundRect(x - width / 2, y, width, 13, 5, 5);
    }

    public final Rectangle getBounds() {
        return new Rectangle(x - width / 2, y, width, 13);
    }

    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (!reversed)
                xa = -2;
            else
                xa = 2;
            isLeftDown = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (!reversed)
                xa = 2;
            else
                xa = -2;
            isRightDown = true;
        }


        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            for (int i = 0; i < game.balls.size(); i++) {
                game.balls.get(i).unstuck();

            }
        }

    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            isLeftDown = false;
            if (!isRightDown)
                xa = 0;
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            isRightDown = false;
            if (!isLeftDown)
                xa = 0;
        }


    }
}