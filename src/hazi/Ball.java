package hazi;

import java.awt.*;

public class Ball {
    double x;
    double y;
    double xa = 1;
    double ya = -1;
    double txa = -1000;
    double tya = -1000;
    double tx;
    static int r = 15;
    protected GamePanel game;
    static double gameSpeed = 1;
    boolean stuck = false;

    public Ball(GamePanel game) {
        this.game = game;
    }

    void move() {

        x = x + xa * gameSpeed;
        y = y + ya * gameSpeed;
        if (x + xa < 0)
            xa = Math.abs(xa);
        if (x + xa > Game.windowWidth - r)
            xa = -Math.abs(xa);
        if (y + ya < GamePanel.top)
            ya = -ya;

        if (game.racket.sticky && y + 15 < game.racket.y)
            stuck = true;

        if (collision()) {
            if (!stuck) {
                if (txa != -1000 || tya != -1000) {
                    xa = txa;
                    ya = tya;
                    txa = -1000;
                    tya = -1000;

                }
                if (game.getBallNum() == 1)
                    gameSpeed *= 1.02;
                else
                    gameSpeed *= 1.01;
                double speedXY = Math.sqrt(xa * xa + ya * ya);
                double posX = (x + r - game.racket.x) / (game.racket.width / 2);
                final double influenceX = 0.75;
                ya = Math.sqrt(speedXY * speedXY - xa * xa) * -1;
                xa = speedXY * posX * influenceX;

            } else {
                if (txa == -1000 && tya == -1000) {
                    txa = xa;
                    tya = ya;
                    tx = x - game.racket.x;
                }

                xa = 0;
                ya = 0;
                x = game.racket.x + tx;
            }
        }
        if (y + ya > Game.windowHeight - r)
            if (game.getBallNum() == 1) {
                if (GamePanel.lives > 0) {

                    GamePanel.lives--;
                    gameSpeed = 1;
                    game.racket.width = game.racket.widthOriginal;
                    game.racket.reversed = false;
                    game.racket.x = ((Game.windowWidth - 10) / 2);
                    x = (game.racket.x - r / 2);
                    y = game.racket.y - 1 - r;
                    xa = 1;
                    ya = -1;
                    game.extra.clear();
                    game.repaint();
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        //
                    }

                } else {
                    game.pause();
                    game.showHighscore();
                }
            } else {
                game.balls.remove(this);
            }
    }

    boolean collision() {
        return game.racket.getBounds().intersects(getBounds());
    }

    public void paint(Graphics2D g) {
        g.setColor(Color.white);
        g.fillOval((int) x, (int) y, r, r);
        

    }

    public void unstuck() {
        stuck = false;
    }

    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, r, r);
    }

}