package hazi;

import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: Balázs
 * Date: 2013.11.30.
 * Time: 18:55
 * To change this template use File | Settings | File Templates.
 */
public class HighScore extends JPanel implements KeyListener {

    public ArrayList<Score> list;// = new ArrayList<Score>();
    private Game main;
    private Game parent;
    private String name;
    private int actualScore;
    private boolean writed = false;
   
    class ScoreComparator implements Comparator<Score> {
        public int compare(Score score1, Score score2) {
            return score2.score - score1.score;
        }
    }

    @SuppressWarnings("unchecked")
	public HighScore(Game main, Game parent, int actualScore) {
        this.main = main;
        this.parent = parent;
        name = new String("");
        this.actualScore = actualScore;
        if (actualScore < 0)
            writed = true;

        try {
            FileInputStream fin = new FileInputStream("serialized");
            ObjectInputStream oin = new ObjectInputStream(fin);
            list = (ArrayList<Score>) oin.readObject();
            oin.close();
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public int minscore() {
        if (list.size() < 9)
            return 0;
        int minimum = list.get(0).score;

        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).score < minimum)
                minimum = list.get(i).score;
        }
        return minimum;
    }
    
    public boolean isEmpty() {
		return list.isEmpty();
	}

    public void paint(Graphics g) {

        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        setBackground(Color.BLACK);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setFont(new Font("Arial", Font.BOLD, 20));
        g2d.setColor(Color.white);
        if (GamePanel.paused)
            g2d.drawString("Press \"ESC\" to exit game..", 130, 500);
        else
            g2d.drawString("Press \"ESC\" to return to the menu..", 70, 500);

        g2d.setFont(new Font("Arial", Font.BOLD, 30));
        if (GamePanel.paused && (minscore() < actualScore)) {
            g2d.drawString("Enter your name: ", 120, 100);
            g2d.drawString(name, Game.windowWidth / 2 - 10 - name.length() * 10, 140);
        } else {

            for (int i = 0; i < list.size() && i < 9; i++) {
                g2d.drawString(Integer.toString(i + 1) + ". ", 50, i * 50 + 50);
                g2d.drawString(list.get(i).name, 90, i * 50 + 50);
                g2d.drawString(Integer.toString(list.get(i).score) + ". ", 400, i * 50 + 50);
            }
        }
    }

    public void orderList() {
        Collections.sort(list, new ScoreComparator());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if (GamePanel.paused) {
                serialize();
                System.exit(0);
            } else {
                parent.dispose();
                main.setVisible(true);

            }
        }

        if (e.getKeyCode() == KeyEvent.VK_ENTER && !writed) {
            writed = true;
            list.add(new Score(name,actualScore));
            actualScore = 0;
            orderList();
            serialize();
            repaint();
        }


        if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
            if (name.length() > 0) {
                name = name.substring(0, name.length() - 1);
                repaint();
            }

        Character c = Character.toChars(e.getKeyCode())[0];
        if (c >= '0' && c <= 'Z') {
            if (name.length() <= 10) {
                name = name + c;
                repaint();
            }
        }
        JTextField a = new JTextField();
        a.getText();
    }

    public void serialize() {
        try {
            FileOutputStream fout = new FileOutputStream("serialized");
            ObjectOutputStream oout = new ObjectOutputStream(fout);

            oout.writeObject(list);
            oout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //
    }
}
