package hazi;


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Runnable, KeyListener {

    static int sleepTime = 5;

    static int top = 50;
    static int score = 0;
    static int lives = 2;
    boolean initialized = false;
    static boolean paused = false;

    private Game game;
    Racket racket = new Racket(this);

    Blocks blocks = new Blocks(this);
    Text text = new Text(this);
    ArrayList<Extra> extra = new ArrayList<Extra>();
    ArrayList<Ball> balls = new ArrayList<Ball>();

    static boolean isPaused() {
		return paused; 	
    }
    
    public GamePanel(Game game) {
        this.game = game;
        this.setBackground(Color.black);
        setFocusable(true);
    }

    public void init() {

        racket.x = ((Game.windowWidth - 10) / 2);
        racket.y = Game.windowHeight - 50;

        balls.add(new Ball(this));
        balls.get(0).x = (racket.x - Ball.r / 2);
        balls.get(0).y = racket.y - 1 - Ball.r;
    }

    public void move() {

        racket.move();

        racket.check();
        blocks.check();

        for (int i = 0; i < getBallNum(); i++) {
            balls.get(i).move();
        }

        for (int i = 0; i < extra.size(); i++) {
            extra.get(i).move();
            extra.get(i).check();
        }
    }
    
    public int getBallNum() {
    	return balls.size();
    }
    
    @Override
    public void paint(Graphics g) {

        super.paint(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setBackground(Color.black);


        racket.paint(g2d);
        blocks.paint(g2d);
        text.paint(g2d);
        g.setColor(Color.white);

        for (int i = 0; i < balls.size(); i++) {
            balls.get(i).paint(g2d);
        }

        for (int i = 0; i < extra.size(); i++) {
            extra.get(i).paint(g2d);
        }
        if (isPaused()) {
            PauseMenu p = new PauseMenu();
            p.paint(g2d);
        }
    }

    public void gameOver() {
        JOptionPane.showMessageDialog(this, "Game Over", "Game Over", JOptionPane.YES_NO_OPTION);
        System.exit(ABORT);
    }

    public void showHighscore() {
        game.setVisible(false);
        game.repaint();
        game.dispose();

        game.showHighScore(score);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            paused = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
            if (!GamePanel.isPaused())
                pause();
            else {
                showHighscore();
            }
        if (e.getKeyCode() == KeyEvent.VK_SHIFT)

            pause();

        racket.keyPressed(e);

    }

    @Override
    public void keyReleased(KeyEvent e) {
        racket.keyReleased(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    public void pause() {
        paused = true;
        repaint();
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (!initialized) {
                    init();
                    initialized = true;
                }
                if (!isPaused()) {
                    move();
                    repaint();
                }

                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }
}