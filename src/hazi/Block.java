package hazi;

import java.awt.*;


public class Block {
	int x;
	int y;
	int width = 40;
	int height = 13;
	boolean visible = true;
	GamePanel game;
	
	public enum Type {
		normal, big, small, multiball, reverse, sticky
	};

	Type type = Type.normal;

	public Block(int x, int y, GamePanel game) {
		this.x = x;
		this.y = y;
		this.game = game;
	}

	public void paint(Graphics2D g) {
		g.setColor(Color.orange);
		if (visible) {
			g.fillRoundRect(x, y, width, height, 3, 3);
		}
	}

	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, width, height);
	}

	private boolean collision(int i) {
		if (visible)
			if (game.balls.get(i).getBounds().intersects(getBounds()))
				return true;

		return false;
	}



	public void check() {
		for (int i = 0; i < game.getBallNum(); i++) {
			if (collision(i)) {

				GamePanel.score += 10;
				visible = false;
				Blocks.blockNum--;
                if (Blocks.blockNum == 0) {
                	game.pause();
                    Blocks.game.showHighscore();
                }


                Type t;
                switch (type) {
                    case multiball:
                        t = Type.multiball;
                        break;
                    case big:
                        t = Type.big;
                        break;
                    case small:
                        t = Type.small;
                        break;
                    case reverse:
                        t = Type.reverse;
                        break;
                    case sticky:
                        t = Type.sticky;
                        break;
				default:
					t = Type.normal;
					break;
                }

                if (t != Type.normal)
                    Blocks.game.extra.add(new Extra(t,	Blocks.game, x + width / 2, y + height / 2));

				double bx = game.balls.get(i).x + Ball.r;
				double by = game.balls.get(i).y + Ball.r;

				if (x <= bx && x + width >= bx) {
					game.balls.get(i).ya *= -1;
					game.balls.get(i).y += game.balls.get(i).ya;
				} else 
					if (y <= by && y + height >= by) {
						game.balls.get(i).xa *= -1;
						game.balls.get(i).x += game.balls.get(i).xa;
					} else {
						game.balls.get(i).xa *= -1;
						game.balls.get(i).x += game.balls.get(i).xa;
						game.balls.get(i).ya *= -1;
						game.balls.get(i).y += game.balls.get(i).ya;
					}
			}
		}
	}
}
