package hazi;

import java.awt.*;


/**
 * Created with IntelliJ IDEA.
 * User: Balázs
 * Date: 2013.11.30.
 * Time: 18:09
 * To change this template use File | Settings | File Templates.
 */
public class PauseMenu {



    public void paint(Graphics2D g) {
        g.setColor(Color.GRAY);
        int x = 70;
        int w = 350;
        int y = 67;
        int h = 150;
        g.fillRoundRect(x, y, w, h, 5, 5);
        g.setColor(Color.black);
        g.fillRoundRect(x + 5, y + 5, w - 10, h - 10, 5, 5);
        g.setColor(Color.white);
        Graphics2D g2d = (Graphics2D) g;

        g2d.drawString("Press \"ENTER\" to continue.", x + 30, y + 50);
        g2d.drawString("Press \"ESC\" again to exit.", x + 30, y + 110);

    }
}
