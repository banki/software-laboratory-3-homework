package hazi;

import java.awt.Graphics2D;
import java.util.Random;

import hazi.Block.Type;

public class Blocks {
    static int sor = 10;
    static int oszlop = 10;
    static int blockNum = sor * oszlop;
    Block block[][] = new Block[sor][oszlop];
    public static GamePanel game;
    
    public Blocks(GamePanel game) {
        Blocks.game = game;
        Random generator = new Random(System.currentTimeMillis());
        int r;
        for (int i = 0; i < sor; i++) {
            for (int j = 0; j < oszlop; j++) {


                block[i][j] = new Block(10 + (i * 30) + (i * 18), GamePanel.top + j
                        * 10 + j * 10,game);
                r = generator.nextInt(10);

                switch (r) {
                    case 1:
                        block[i][j].type = Type.multiball;
                        break;
                    case 2:
                        block[i][j].type = Type.big;
                        break;
                    case 3:
                        block[i][j].type = Type.small;
                        break;
                    case 4:
                        block[i][j].type = Type.reverse;
                        break;
                    case 5:
                        block[i][j].type = Type.sticky;
                        break;
                    default:
                        block[i][j].type = Type.normal;
                        break;
                }
            }
        }
    }

    public void paint(Graphics2D g) {
        for (int i = 0; i < sor; i++) {
            for (int j = 0; j < oszlop; j++) {
                block[i][j].paint(g);
            }
        }
    }

    public void check() {
        for (int i = 0; i < sor; i++) {
            for (int j = 0; j < oszlop; j++) {
                block[i][j].check();
            }
        }
    }
}
